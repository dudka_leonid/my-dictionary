package com.mydictionary.JSON

import android.util.Log
import com.mydictionary.JSON.Word
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.json.JsonDecodingException
import kotlinx.serialization.list
import java.io.File
import java.lang.Exception

class DictionaryManagerClass(path: File) {

    private var file: File

    init {
        file = File(path, FILE_NAME)
        Log.e("WordListTest", file.toString())
    }

    private fun doSerializingObjects(words: List<Word>): String {
        return Json(JsonConfiguration.Stable).stringify(Word.serializer().list, words)
    }

    private fun doUser(stringValue: String): List<Word> {
        try {
            return Json(JsonConfiguration.Stable).parse(Word.serializer().list, stringValue)
        }catch (e: JsonDecodingException){
            Log.e("DEBUG_MyJson", e.toString())
            throw e
        }
    }

    fun saveData(words: List<Word>){
        file.writeText(doSerializingObjects(words))
    }

    fun loadData(): List<Word>{
        return if (file.exists()){
            doUser(file.readText())
        }else{
            saveData(ArrayList())
            return ArrayList()
        }
    }

    fun deleteFile(){
        file.delete()
    }

    companion object {
        const val FILE_NAME: String = "data.txt"
    }
}