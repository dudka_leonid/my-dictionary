package com.mydictionary.JSON

import kotlinx.serialization.Serializable

@Serializable
data class Word(val id:Long = 0, val en: String, val ru: ArrayList<String>){
    override fun toString(): String {
        return "$id:$en:$ru"
    }
}
/*
* fun String.ToWord(): Word {
        val parse = this.split(":") as ArrayList<String>
        val ru = parse[2].drop(1).dropLast(1).split(", ") as ArrayList<String>
        return Word(parse[0].toLong(), parse[1], ru)
    }
* */