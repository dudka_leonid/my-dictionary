package com.mydictionary

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.mydictionary.JSON.Word
import com.mydictionary.fragments.MainFragmentsAdapter
import com.mydictionary.fragments.WordListFragment
import com.mydictionary.fragments.dialog_fragments.EditWordViewDialog


class MainActivity : AppCompatActivity(), EditWordViewDialog.InterfaceCommunicator {

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager
    private lateinit var mainFragmentsAdapter: MainFragmentsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {

            tabLayout = findViewById(R.id.sliding_tabs)
            viewPager = findViewById(R.id.view_pager)

            mainFragmentsAdapter = MainFragmentsAdapter(supportFragmentManager, tabLayout.tabCount)
            viewPager.adapter = mainFragmentsAdapter

            viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

            tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    viewPager.currentItem = tab.position
                }
                override fun onTabUnselected(tab: TabLayout.Tab) {
                }
                override fun onTabReselected(tab: TabLayout.Tab) {
                }
            })
            viewPager.currentItem = 2
        }
    }

//----------------------------------------------------------------------------------------------------------------------
    interface OnActivityDataListener {
        fun onActivityDataListener(code: Int, id: Long?, word: Word?)
    }
    private var mListener: OnActivityDataListener? = null
    override fun sendRequestCode(code: Int, id: Long?, word: Word?) {
        mListener = getFragmentFromViewpager() as WordListFragment
        mListener!!.onActivityDataListener(code, id, word)
    }
    private fun getFragmentFromViewpager(): Fragment {
        return mainFragmentsAdapter.instantiateItem(viewPager, viewPager.currentItem) as Fragment
    }
//----------------------------------------------------------------------------------------------------------------------
}
