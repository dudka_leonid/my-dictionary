package com.mydictionary.study_words

import android.app.Activity
import android.content.Context
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.mydictionary.JSON.DictionaryManagerClass
import com.mydictionary.JSON.Word
import com.mydictionary.R
import com.mydictionary.SQLite.DatabaseAdapter
import kotlinx.android.synthetic.main.study_words_fragment.view.*
import android.view.inputmethod.InputMethodManager.HIDE_NOT_ALWAYS
import android.content.Context.INPUT_METHOD_SERVICE
import android.graphics.Color
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService



/* ToDo
    1. Сделать логику для изучения
    английских слов!!!
    2. Отрефактори прошлый код!!!
 */
class StudyWords : Fragment() {

    private lateinit var databaseAdapter: DatabaseAdapter
    private lateinit var startGameLinearLayout: LinearLayout
    private lateinit var playGameLinearLayout: View
    private var n: Int = 10
    private lateinit var adapterStudyWords: StudyWordsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.study_words_fragment, container, false)
        databaseAdapter = DatabaseAdapter(context!!)

        startGameLinearLayout = view.start_game_questions_linearLayout_id
        playGameLinearLayout = view.game_linearLayout_id

        view.start_game_questions_button_id.setOnClickListener {
            this@StudyWords.hideKeyboard()
            adapterStudyWords = StudyWordsAdapter(getRandomWords(n))
            view.study_list_recyclerView_id.adapter = adapterStudyWords
            startGameLinearLayout.visibility = View.GONE
            playGameLinearLayout.visibility = View.VISIBLE
            view.validate_words_button_id.visibility = View.VISIBLE
        }

        view.validate_words_button_id.setOnClickListener {
            this@StudyWords.hideKeyboard()
            adapterStudyWords.listVH.forEach {
                it.wordRu.isFocusable = false
            }
            checkAnswers()
            view.validate_words_button_id.visibility = View.INVISIBLE
            startGameLinearLayout.visibility = View.VISIBLE
        }

        return view
    }

    fun getRandomWords(n: Int): List<Word>{
        val wordsList = ArrayList<Word>()
        databaseAdapter.open()
        val words = databaseAdapter.getWords()
        databaseAdapter.close()
        words.shuffle()
        for (i in words.take(n)){
            wordsList.add(i)
        }
        Log.e("Test_StudyWords",wordsList.toString())
        return wordsList
    }
    fun Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

//    fun Activity.hideKeyboard() {
//        if (currentFocus == null) View(this) else currentFocus?.let { hideKeyboard(it) }
//    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun checkAnswers(){
        adapterStudyWords.listVH.forEach {
            Log.e("Test_StudyWords", it.wordRu.text.toString() + " --- " + it.wordEn.text.toString())
            if (it.wordRu.text.toString() == it.wordEn.text.toString()){
                it.wordRu.setTextColor(Color.GREEN)
            }else{
                it.wordRu.setTextColor(Color.RED)
            }
        }
    }

}