package com.mydictionary.study_words

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mydictionary.JSON.Word
import com.mydictionary.R
import java.util.*
import kotlin.collections.ArrayList

class StudyWordsAdapter(private val words: List<Word>): RecyclerView.Adapter<StudyWordsAdapter.ViewHolderWord>() {

    var answers: ArrayList<String> = ArrayList(Collections.nCopies(words.size, ""))
        private set

    var listVH = ArrayList<ViewHolderWord>()
        private set

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderWord {
       val layoutView = LayoutInflater.from(parent.context)
           .inflate(R.layout.item_study_game_ino_list, parent, false)
        val layoutViewVH = ViewHolderWord(layoutView)
        listVH.add(layoutViewVH)
        return layoutViewVH
    }

    override fun getItemCount(): Int {
        return words.size
    }
    override fun onBindViewHolder(holder: ViewHolderWord, position: Int) {
        holder.bind(words[position])

        holder.wordRu.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                answers[position] = s.toString().trim()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    class ViewHolderWord(itemView: View): RecyclerView.ViewHolder(itemView){

        var wordEn: TextView = itemView.findViewById(R.id.item_word_en_textView_id)
            private set

        var wordRu: EditText = itemView.findViewById(R.id.item_word_ru_editText_id)
            private set

        fun bind(word: Word) {
            wordEn.text = word.en
        }
    }
}