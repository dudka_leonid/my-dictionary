package com.mydictionary.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.mydictionary.JSON.Word
import com.mydictionary.MainActivity
import com.mydictionary.R
import com.mydictionary.SQLite.DatabaseAdapter
import com.mydictionary.fragments.dialog_fragments.EditWordViewDialog
import com.mydictionary.fragments.logic.WordListAdapter
import kotlinx.android.synthetic.main.word_list_fragment.view.*

class WordListFragment: Fragment(), MainActivity.OnActivityDataListener {

    private lateinit var recyclerView_id: RecyclerView
    private lateinit var searchView: SearchView
    private lateinit var adapter_user: WordListAdapter
    private lateinit var dictionary: ArrayList<Word>

    private lateinit var databaseAdapter: DatabaseAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.word_list_fragment, container, false)


        databaseAdapter = DatabaseAdapter(context!!)

        recyclerView_id = view.recyclerView_id
        loadData()
        searchView = view.findViewById(R.id.searchView_id)
        searchView.setQuery("", false)

        searchWordRecyclerView(searchView)

        view.add_new_word_at_list_button_id.setOnClickListener(addNewWord())

        return view
    }

    private fun searchWordRecyclerView(searchView: SearchView) {

        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(text: String?): Boolean {
                adapter_user.filter.filter(text)
                return true
            }
        })
    }

    private fun loadData(){
        databaseAdapter.open()
        dictionary = databaseAdapter.getWords()
        Log.e("asddf235sdf", "Get all words: " + databaseAdapter.getWords().toString())
        databaseAdapter.close()
        adapter_user= WordListAdapter(dictionary)
        recyclerView_id.adapter = adapter_user
    }

    private fun addNewWord(): View.OnClickListener {
        return View.OnClickListener {
          val dialog = EditWordViewDialog()
            dialog.show(fragmentManager!!.beginTransaction(), "EditWordViewDialog")
            dialog.setTargetFragment(this, 1)
        }
    }

    override fun onResume() {
        Log.e("WordListTest","onResume")
        super.onResume()
    }

//----------------------------------------------------------------------------------------------------------------------
    override fun onActivityDataListener(code: Int, id: Long?, word: Word?) {
        when(code){
            // Add Word
            1 -> {
                if (word != null){
                    databaseAdapter.open()
                    dictionary.add(word)
                    databaseAdapter.insert(word)
                    databaseAdapter.close()
                    adapter_user.notifyItemInserted(dictionary.indexOf(word))
                }
            }
            // Change Word
            2 -> {
                if (id != null && word != null){
                    databaseAdapter.open()
                    dictionary[(id.toInt()-1)] = word
                    databaseAdapter.update(word)
                    databaseAdapter.close()
                    adapter_user.notifyItemChanged(dictionary.indexOf(word))
                }
            }
            // Remove Word
            3 -> {
                if (id != null && word != null){
                    adapter_user.notifyItemRemoved(dictionary.indexOf(word))
                    databaseAdapter.open()
                    dictionary.removeAt(dictionary.indexOf(word))
                    databaseAdapter.delete(id)
                    databaseAdapter.close()
                }
            }
        }
    }
//----------------------------------------------------------------------------------------------------------------------
}
