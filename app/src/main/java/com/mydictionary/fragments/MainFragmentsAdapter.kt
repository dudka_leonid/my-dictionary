package com.mydictionary.fragments

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.mydictionary.study_words.StudyWords

class MainFragmentsAdapter(fm: FragmentManager,
                           private var totalTabs: Int): FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> {
                WordListFragment()
            }
            1 -> {
                StudyWords()
            }
            2 -> {
                HomeFragment()
            }
            3 -> {
                SettingsFragment()
            }
            else -> null
        }
    }

    override fun getCount(): Int {
        return totalTabs
    }

}