package com.mydictionary.fragments.dialog_fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.mydictionary.JSON.Word
import com.mydictionary.R
import kotlinx.android.synthetic.main.edit_word_recyclec_view.view.*

class EditWordViewDialog(
    private var word: Word? = null
) : DialogFragment(){

    private lateinit var enTextView: EditText
    private lateinit var ruTextView: EditText

    var mCallback : InterfaceCommunicator? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.edit_word_recyclec_view, container, false)

        try {
            mCallback = activity as InterfaceCommunicator?
        } catch (e: ClassCastException) {
            throw e
        }

        enTextView = view.findViewById(R.id.word_text_input_en)
        ruTextView = view.findViewById(R.id.word_text_input_ru)

        if (word != null){

            enTextView.setText(word!!.en)
            var newWordText = ""
            word!!.ru.forEach {
                newWordText += "$it,"        }
            newWordText = newWordText.dropLast(1)
            ruTextView.setText(newWordText)
        }

        view.btn_dialog_accept.setOnClickListener {
            if (word != null){
                changeWord()
            }else{
                createNewWord()
            }
            dialog.dismiss()
        }

        view.btn_dialog_delete.setOnClickListener {
            removeWord()
            dialog.dismiss()
        }

        return view
    }

    private fun createNewWord(){
        val newWord = Word(en = enTextView.text.toString(), ru = parseWordListRu())
        mCallback!!.sendRequestCode(1, null, newWord)
    }

    private fun changeWord(){
        val newWord = Word(id = word!!.id,en = enTextView.text.toString(), ru = parseWordListRu())
        mCallback!!.sendRequestCode(2, null, newWord)
    }

    private fun removeWord(){
        mCallback!!.sendRequestCode(3, word!!.id, word)
    }

    private fun parseWordListRu(): ArrayList<String> {
        return if (ruTextView.text.contains(',')) {
            ruTextView.text.toString().split(",") as ArrayList<String>
        } else {
            arrayListOf(ruTextView.text.toString())
        }
    }

    interface InterfaceCommunicator {
        fun sendRequestCode(code: Int, id: Long?, word: Word?)
    }
}