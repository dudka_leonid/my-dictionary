package com.mydictionary.fragments.dialog_fragments

import com.mydictionary.JSON.Word

fun main() {
    var list = ArrayList<Word>()
    list.add(Word(1,"en_1", arrayListOf("ru_1", "ru_2")))
    list.add(Word(2,"en_2", arrayListOf("ru_1", "ru_2")))
    list.add(Word(3,"en_3", arrayListOf("ru_1", "ru_2")))

    val newList = ArrayList<Word>()
    list.ToString().ToListWordString().forEach {
        newList.add(it.ToWord())
    }
}

fun String.ToWord(): Word {
    val parse = this.split(":") as ArrayList<String>
    val ru = parse[2].drop(1).dropLast(1).split(", ") as ArrayList<String>
    return Word(parse[0].toLong(), parse[1], ru)
}

fun List<Word>.ToString(): String {
    return this.joinToString(separator = ";")
}

fun String.ToListWordString(): ArrayList<String> {
    return this.split(";") as ArrayList<String>
}