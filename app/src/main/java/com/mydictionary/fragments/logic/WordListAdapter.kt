package com.mydictionary.fragments.logic

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mydictionary.JSON.Word
import com.mydictionary.MainActivity
import com.mydictionary.R
import com.mydictionary.fragments.dialog_fragments.EditWordViewDialog

class WordListAdapter(
    val words: ArrayList<Word>
):
    RecyclerView.Adapter<WordListAdapter.VH_word>(), Filterable {

    var listFiltered: MutableList<Word> = words

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH_word {
        val layoutView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_word_list, parent, false)
        val holder = VH_word(layoutView)
        return holder
    }

    override fun getItemCount(): Int {
        return listFiltered.size
    }

    override fun onBindViewHolder(holder: VH_word, position: Int) {
        holder.bind(listFiltered[position])
    }

    override fun getFilter(): Filter {
        return object : Filter(){
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString: String = constraint.toString()

                if (charString.isEmpty()){
                    listFiltered = words
                }else{
                    val filteredList: MutableList<Word> = mutableListOf()
                    for (s: Word in words){
                        if (s.en.toLowerCase().contains(charString.toLowerCase())){
                            filteredList.add(s)
                        }
                    }
                    listFiltered = filteredList
                }
                val filteredResults: FilterResults = FilterResults()
                filteredResults.values = listFiltered
                return filteredResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                listFiltered = results!!.values as MutableList<Word>
                notifyDataSetChanged()
            }
        }
    }

    class VH_word(itemView: View) : RecyclerView.ViewHolder(itemView){

        private var wordEn: TextView = itemView.findViewById(R.id.word_en_id_textView)
        private var wordRu: TextView = itemView.findViewById(R.id.word_ru_id_textView)

        fun bind(word: Word) {
            wordEn.text = word.en
            if(word.ru.size == 0){
                wordRu.text = "Добавьте перевод"
            }else{
                var newWordText = ""
                word.ru.forEach {
                    newWordText += "$it,"
                }

                wordRu.text = newWordText.dropLast(1)
            }

            itemView.setOnClickListener {
                EditWordViewDialog(word)
                    .show((itemView.context as MainActivity).supportFragmentManager,
                        "com.mydictionary.fragments.dialog_fragments.EditWordViewDialog")
            }
        }
    }
}

