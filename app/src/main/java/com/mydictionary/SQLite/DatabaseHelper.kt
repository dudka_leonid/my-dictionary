package com.mydictionary.SQLite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHelper(context: Context)
    :SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "dictionary.db"
        val TABLE_NAME = "dictionary"

        val COLUMN_ID = "_id"
        val COLUMN_EN = "en"
        val COLUMN_RU = "ru"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("CREATE TABLE " +
                TABLE_NAME + "("
                + COLUMN_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COLUMN_EN  +" TEXT, " +
                COLUMN_RU
                + " TEXT" + ")")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }
}