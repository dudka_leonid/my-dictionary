package com.mydictionary.SQLite

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.DatabaseUtils
import android.database.sqlite.SQLiteDatabase
import com.mydictionary.JSON.Word


class DatabaseAdapter(context: Context) {

    private var dbHelper: DatabaseHelper = DatabaseHelper(context.applicationContext)
    private lateinit var database: SQLiteDatabase

    // Открытие потока БД
    fun open(): DatabaseAdapter {
        database = dbHelper.writableDatabase
        return this
    }

    // Закрытие потока БД
    fun close(){
        dbHelper.close()
    }

    // получаем все записи
    private fun getAllEntries(): Cursor {
        val columns: Array<String> = arrayOf(
            DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_EN, DatabaseHelper.COLUMN_RU)
        return  database.query(
            DatabaseHelper.TABLE_NAME, columns, null,
            null, null, null, null)
    }

    // Получаем список всех слов
    fun getWords(): ArrayList<Word>{
        val words:ArrayList<Word> = ArrayList()
        val cursor: Cursor = getAllEntries()
        if (cursor.moveToFirst()){
            do {
                val id: Int = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID))
                val en: String = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_EN))
                val ru: String = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_RU))

                val x = if (ru.contains(",")){
                    ru.drop(1).dropLast(1).split(", ") as ArrayList<String>
                }else{
                   arrayListOf(ru.drop(1).dropLast(1))
                }
                
                words.add(Word(id.toLong(), en,x))
            }while (cursor.moveToNext())
        }
        cursor.close()
        return words
    }

    // Получаем кол-во слов
    fun getCount(): Long{
        return DatabaseUtils.queryNumEntries(database, DatabaseHelper.TABLE_NAME)
    }

    // Получаем слово по id
    fun getWord(id: Long): Word?{
        var word: Word? = null
        val query: String = String
            .format("SELECT * FROM %s WHERE %s=?", DatabaseHelper.TABLE_NAME, DatabaseHelper.COLUMN_ID)
        val cursor: Cursor = database.rawQuery(query, arrayOf(id.toString()))
        if (cursor.moveToFirst()){
            val en: String = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_EN))
            val ru: String = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_RU))
            word = Word(id,en,
                ru.drop(1).dropLast(1).split(", ") as ArrayList<String>)
        }
        cursor.close()
        return word
    }

    fun insert(word: Word): Long{
        val cv: ContentValues = ContentValues()

        cv.put(DatabaseHelper.COLUMN_EN, word.en)
        cv.put(DatabaseHelper.COLUMN_RU, word.ru.toString())

        return database.insert(DatabaseHelper.TABLE_NAME, null, cv)
    }

    fun delete(userId: Long): Int{
        val whereClause: String = "_id = ?"
        val whereArgs: Array<String> = arrayOf(userId.toString())
        return database.delete(DatabaseHelper.TABLE_NAME, whereClause, whereArgs)
    }

    fun deleteAll(): Boolean{
        getWords().forEach {
            delete(it.id)
        }
        return getCount() == 0L
    }

    fun update(word: Word): Int{
        val whereClause: String = DatabaseHelper.COLUMN_ID + "=" + word.id.toString()
        val cv: ContentValues = ContentValues()
        cv.put(DatabaseHelper.COLUMN_EN, word.en)
        cv.put(DatabaseHelper.COLUMN_RU, word.ru.toString())

        return database.update(DatabaseHelper.TABLE_NAME, cv, whereClause, null)
    }

    fun checkRecord(word: Word): Boolean {
        var ch = false
        val columns: Array<String> = arrayOf(
            DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_EN, DatabaseHelper.COLUMN_RU)
        val cursor =  database.query(
            DatabaseHelper.TABLE_NAME, columns,
            DatabaseHelper.COLUMN_EN+" = ?", arrayOf(word.en),
            null, null, null, null)
        if (cursor.count > 0) ch = true
        cursor.close()
        return ch
    }

    fun getId(en: String): Int {
        var id: Int = 0
        val query: String = String
            .format("SELECT * FROM %s WHERE %s=?", DatabaseHelper.TABLE_NAME, DatabaseHelper.COLUMN_EN)
        val cursor: Cursor = database.rawQuery(query, arrayOf(en))
        if (cursor.moveToFirst()){
            id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID))
        }
        cursor.close()
        return id
    }
}